# README #

### Arduino Project for AirQualitySystem ###

This is a small project developed my during the Embedded System course at University.
The idea is to develop a portable system that collect air quality data with GPS time and position
to build up an air quality map of Trento.
The system will be cheap, about 50�.

### Project Components ###

* ATMEGA328
* Micro SD module
* ESP8266 WiFi Module
* NEO-6M GPS Module
* Two Air quality sensors
* One DustSensor

### Who do I talk to? ###

* Matteo Dallapiccola
* matteo.dallapiccola@gmail.com