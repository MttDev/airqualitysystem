// Coordinate pixel 0,0 = (11.0532267857;46.12713332342008);
// Incremento per pixel: DX = 0.0001441571428714;
// Incremento per pixel: DY = 0.000099845105328;

PImage map;
float imgZoom = 1;
int imgXPose = -55;
int imgYPose = -55;

float offsetX = 11.0532267857;
float offsetY = 46.12713332342008;

float deltaX = 0.0001441571428714;
float deltaY = -0.000099845105328;

String[] sensorData;

int poseX = 0;
int poseY = 0;
int PM01 = 0;
int PM25 = 0;
int PM10 = 0;

int exe = 0;

PrintWriter output;

void setup() {
  colorMode(HSB, 100);
  // Genera la finestra
  size(1000, 1000);
  fill(255);
  stroke(0);
  
  output = createWriter("DataS.js");
  
  
  // Carica la mappa della città
  map = loadImage("Map.jpg");
  
  // Carica il file di dati
  sensorData = loadStrings("Data.txt");
}

void draw() {
  colorMode(HSB, 100);
  if (exe == 0) {
    // Disegna la mappa iniziale
    image(map, imgXPose, imgYPose, 1000.0*imgZoom, 1000.0*imgZoom);
    output.print("eqfeed_callback({\"features\":[");
    for (int i = 0; i<sensorData.length; i++ ) {
      String[] sensorString = split(sensorData[i], '#');
      poseX = poseToPixelX(float(sensorString[8]));
      poseY = poseToPixelY(float(sensorString[7]));
      if (sensorString.length >10) {
        
        output.println("{\"lat\":" + sensorString[7] + ",\"lng\":" + sensorString[8] + ",\"data\":" + sensorString[11] + "},");
        //println(sensorString[9]);
        int PMtot = int(sensorString[9]) + int(sensorString[10]) + int(sensorString[11]);
        

        int hue = int(30-((PMtot*1.2 - 40)*0.3));
        color pixColor = color(hue, 100, 100);
        for ( int ii = -2; ii <= 2; ++ii) {
          for ( int ij = -2; ij <= 2; ++ij) {
          
            
        set(poseX+ii, poseY+ij, pixColor);
          }
        }
      }
    }
    output.print("]});");
    output.flush();
    output.close();
    exe = 1;
  }
  
  
}

int poseToPixelX(float poseX) {
  float pixX = poseX - offsetX;
  pixX = pixX / (deltaX/imgZoom) + imgXPose;
  return int(pixX);  
}

int poseToPixelY(float poseY) {
  float pixY = poseY - offsetY;
  pixY = pixY / (deltaY/imgZoom) +imgYPose;
  return int(pixY);  
}

void keyPressed() {
 if (key == CODED) {
  if (keyCode == LEFT) {
    imgXPose = imgXPose - 10;
  } else if (keyCode == RIGHT) {
    imgXPose = imgXPose + 10;
    
  } else if (keyCode == UP) {
    imgYPose = imgYPose - 10;
    
  } else if (keyCode == DOWN) {
    imgYPose = imgYPose + 10;
  }
   
 } else {
   if (key == '+') {
     imgZoom = imgZoom + 0.1;
   } else if (key == '-') {
     
     imgZoom = imgZoom - 0.4;
   } else if (key == 'a') {
     offsetX = offsetX + 0.00001;
   } else if (key == 'd') {
     offsetX = offsetX - 0.00001;
     
   } else if (key == 'w') {
     offsetY = offsetY - 0.00001;
   } else if (key == 's') {
     offsetY = offsetY + 0.00001;
     
   }
 }
  
}