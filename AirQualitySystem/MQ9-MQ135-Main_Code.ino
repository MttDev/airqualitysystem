#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <SD.h>
#include <Arduino.h>

// SD CARD Configuration
// Connected pin 5
int datasetLength = 0;
int datasetNumA = 0;
String datasetNum = "";                               
File dataFile;

// WIFI MODULE Configuration
SoftwareSerial WIFI_com(9, 10); 
String DataStringGPS;
String DataStringDUST;

// GPS Configuration
TinyGPSPlus GPS;
SoftwareSerial GPS_com(8, 9); //RXPin = 4, TXPin = 3;

String gpsStream = "";

// DUST SENSORS CONFIGURATION
#define MQ9sensor A5;     // Analog Input 5
#define MQ135sensor A3;   // Analog Input 3

int STATE = 1;
// 0 = IDLE
// 1 = CHECK
// 2 = READ GPS DATA
// 3 = READ DUST SENSOR
// 4 = WRITE DATA ON SD CARD
// 5 = SEND DATA ON WIFI
// 6 = ERROR STATE

void setup() {

  delay(5000);
  
  // SD CARD SETUP (on pin 5)
  datasetNumA = 0;
  datasetNum = String(datasetNumA) + ".txt";
  SD.begin(5);
  // Find the next log file
  int testSD = 1;
  while (testSD == 1) {
    datasetNum = String(datasetNumA) + ".txt";
    if (SD.exists(datasetNum)) {
      datasetNumA++;
    } else {
      testSD = 0;
    }
  }
  
  // DUST SENSOR SETUP
  Serial.begin(9600);
  Serial.setTimeout(1500);

  // GPS SETUP
  GPS_com.begin(9600); 
  
}

void loop() {
  
  switch (STATE) {
  case 0: // IDLE
    idle();
    break;
  case 1: // CHECK
    check();
    break;
  case 2: // READ GPS DATA
    readGPS();
    break;
  case 3: // READ DUST SENSOR
    readDust();
    break;
  case 4: // WRITE DATA ON SD CARD
    writeData();
    Serial.print(DataStringGPS);
    Serial.println(DataStringDUST);
    break;
  case 5: // SEND DATA OVER WIFI
    sendData();
    break;
  case 6: // ERROR STATUS
    errorStatus();
    break;
  }
  
}

void idle() {
  
  STATE = 2;

}

void check() {
  
  delay(1000);
  STATE = 0;
   
}

void readGPS() {
  
  while(GPS_com.available() > 0) {
    char t = GPS_com.read();
  }
  
  gpsStream ="";
  bool readData = true;
  char ch_data = "";
  int lineRead = 0;
  while (readData) {
    while (GPS_com.available()) {
      ch_data = GPS_com.read();
      gpsStream.concat(ch_data);
      if (ch_data == '\n') {
        lineRead++;
      }
      if (lineRead >= 8) {
        readData = false;
      }
    }
  }
  
  for (int i = 0; i<gpsStream.length(); ++i) {
    char aa = gpsStream.charAt(i);
    GPS.encode(aa);
  }
  
  DataStringGPS = GPS.location.isValid();
  DataStringGPS += '#';
  DataStringGPS += GPS.date.year();
  DataStringGPS += '#';
  DataStringGPS += GPS.date.month();
  DataStringGPS += '#';
  DataStringGPS += GPS.date.day();
  DataStringGPS += '#';
  DataStringGPS += GPS.time.hour();
  DataStringGPS += '#';
  DataStringGPS += GPS.time.minute();
  DataStringGPS += '#';
  DataStringGPS += GPS.time.second();
  DataStringGPS += '#';
  DataStringGPS += String(GPS.location.lat(), 8);
  DataStringGPS += '#';
  DataStringGPS += String(GPS.location.lng(), 8);
  DataStringGPS += '#';

  STATE = 3;
  
}

void readDust() {
  
  DataStringDUST = "";
  while (DataStringDUST == "") {

    // MQ9 Sensor
    float MQ9_R0 = 0.06;   // Calibrated R0
    
    float MQ9_Rs;          // Get value of RS in a GAS
    float MQ9_value;       // Analog Value
    float MQ9_volt;        // Output voltage out of electric divider
    float MQ9_ratio;       // Ratio Rs/R0 used to find pollutants in ppm

    //--- Get a average data by testing 100 times ---//
    for(int x = 0 ; x < 100 ; x++)
    {
        MQ9_value = MQ9_value + analogRead(A5);
    }
    MQ9_value = MQ9_value/100.0;
    //-----------------------------------------------//

    //MQ9_value = analogRead(A5);

    MQ9_volt = (float)MQ9_value / 1024 * 5.0;
    MQ9_Rs = ( 5.0 - MQ9_volt ) / MQ9_volt;
    MQ9_ratio = MQ9_Rs / MQ9_R0;

    float MQ9_CO_ppm = CO_ppm( MQ9_ratio );

    DataStringDUST += MQ9_CO_ppm;
    DataStringDUST += "#";

    // MQ135 Sensor
    float MQ135_R0 = 7.9;   // Calibrated R0
    
    float MQ135_Rs;          // Get value of RS in a GAS
    float MQ135_value;       // Analog Value
    float MQ135_volt;        // Output voltage out of divider
    float MQ135_ratio;       // Ratio Rs / R0 used to find pollutants in ppm

    //--- Get a average data by testing 100 times ---//
    for(int x = 0 ; x < 100 ; x++)
    {
        MQ135_value = MQ135_value + analogRead(A3);
    }
    MQ135_value = MQ135_value/100.0;
    //-----------------------------------------------//

    //MQ135_value = analogRead(A3);
  
    MQ135_volt = (float)MQ135_value / 1024 * 5.0;
    MQ135_Rs = ( 5.0 - MQ135_volt ) / MQ135_volt;
    MQ135_ratio = MQ135_Rs / MQ135_R0;

    float MQ135_CO2_ppm = CO2_ppm( MQ135_ratio );

    DataStringDUST += MQ135_CO2_ppm;
    DataStringDUST += "#";
   
  }
  STATE = 4;
  
}

void writeData() {
  
  dataFile = SD.open(datasetNum, FILE_WRITE);
  dataFile.print(DataStringGPS);
  dataFile.println(DataStringDUST);
  dataFile.close();
  STATE = 0;
  datasetLength++;
  if (!SD.exists(datasetNum)) {
    STATE = 6;
  }
  if (datasetLength >= 5000) {
    datasetLength = 0;
    datasetNumA++;
    datasetNum = String(datasetNumA) + ".txt";    
  }
}

void sendData() {
  Serial.print(DataStringGPS);
  Serial.println(DataStringDUST);
  STATE = 0;
}

void errorStatus() {
  
  // Return to check mode
  STATE = 1;
    
}

float CO_ppm( float RsR0 ){

  /* Here I calculate the amount of CO taking account of RsR0 ratio measured by MQ-9 
   *  sensor. I suppose that there is no LPG and methane in the sorrounding.
   */

  // Fitting values of characteristic curve
  float coeff_a = 581.5001;
  float coeff_b = -2.2053;

  // Characteristic curve from datasheet
  float ppm = coeff_a * powf( RsR0 , coeff_b ); // Characteristic curve from datasheet

  return ppm;
    
}

float CO2_ppm( float RsR0 ){

  /* I calculate the amount of CO2 taking account of RsR0 ratio measured by MQ-135 
   *  sensor. I suppose that there's no other gases that can be acquired in the environment.*/

  // Fitting values of characteristic curve
  float coeff_CO2_a = 109.1175;
  float coeff_CO2_b = -2.7927;

  // Characteristic curve from datasheet
  float ppm = coeff_CO2_a * powf( RsR0 , coeff_CO2_b);

  /* Divide CO2 values in grups which interval is equal to Delta, in order to easily 
  represent data on map. If you want not to apply this, comment the following for construct*/
  int DeltaValue = 400;
  int MaxMultiplicator = 40;
  
  for( int j = 0 ; j < MaxMultiplicator ; j++){

    if( ppm < DeltaValue / 2 + j * DeltaValue ){

      if( fabs( ppm - j * DeltaValue ) < DeltaValue  ){
        ppm = j * DeltaValue;
        break;
      }
      
    }
    
  }
  
  
  return ppm;
    
}

