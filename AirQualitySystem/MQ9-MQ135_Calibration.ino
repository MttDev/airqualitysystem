/*********************/

/* 
CALIBRATION CODE: HERE WE CALIBRATE THE MQ-9 and MQ-135 sensors.
It's necessary to stay in a clean environment in order to have good calibration,
Obviously in a fixed position. In order to take the characteristic resistance of sensors
you must divide the resistance found for a standard value made by the graph of gases ppm related to
Rs/R0 ratio.
 
MQ-9 sensor is calibrated referring to carbon monoxide, considering as the CO quantity in clean air
equal to 0.1 ppmv (mean atmosferic value, https://en.wikipedia.org/wiki/Carbon_monoxide#cite_note-47):
since we have not a more sensitive sensor for a comparison, we must do so. Taking the sensor datasheet,
we extrapolate the ppm to RsR0 curve of CO, in order to take the RsRo value taking account of 0.1 ppmv: 
then from calibration we use the measured Rs value and dividing it by RsR0 we finally find the R0 value
which can be used for the future measures.

MQ-135 sensor is calibrated in the same way as the previous, only for carbon dioxide and taking as
reference a value equal to 402 ppmv (mean atmospheric value, https://en.wikipedia.org/wiki/Carbon_dioxide).

Calibration data can be stored on an SD card connected to the Arduino, in the same manner as the
Main code.
*/

/*********************/

#include <SoftwareSerial.h>
#include <SPI.h>
#include <SD.h>
#include <Arduino.h>

// SD CARD Configuration
// Connected pin 5
int datasetLength = 0;
int datasetNumA = 0;
String datasetNum = "";                               
File dataFile;
String DataString;

void setup() {

    delay(5000);
    
    // SD CARD SETUP (on pin 5)
    datasetNumA = 0;
    datasetNum = String(datasetNumA) + ".txt";
    SD.begin(5);

    // Trova il file di log successivo
    int testSD = 1;
    while (testSD == 1) {
     datasetNum = String(datasetNumA) + ".txt";
     if (SD.exists(datasetNum)) {
       datasetNumA++;
      } else {
        testSD = 0;
     }
    }
  
    Serial.begin(9600);
}

void loop() {

    //*********** MQ9-sensor ************//
    
    float MQ9_sensor_volt;
    float MQ9_RS_air; //  Get the value of RS via in a clear air
    float MQ9_R0;  // Get the value of R0 via in LPG
    float MQ9_sensorValue;
    float MQ9_CO_ppm;
    float MQ9_Clean_Ratio;
    
    //--- Get a average data by testing 100 times ---//
    for(int x = 0 ; x < 100 ; x++)
    {
        MQ9_sensorValue = MQ9_sensorValue + analogRead(A5);
    }
    MQ9_sensorValue = MQ9_sensorValue/100.0;
    //-----------------------------------------------//

    MQ9_sensor_volt = MQ9_sensorValue/1024*5.0;
    MQ9_RS_air = (5.0-MQ9_sensor_volt)/MQ9_sensor_volt; // omit *RL
    
    MQ9_CO_ppm = 0.1;  // Mean atmosfreic CO value (ppmv) as calibration reference

    MQ9_Clean_Ratio = powf( MQ9_CO_ppm / 581.5001 , 1 / -2.2053 );

    MQ9_R0 = MQ9_RS_air / MQ9_Clean_Ratio;


    Serial.print("MQ9_RS_air = ");
    Serial.print(MQ9_RS_air);
    Serial.println("kOhm");

    Serial.print("MQ9_Clean_Ratio = ");
    Serial.println(MQ9_Clean_Ratio);

    Serial.print("MQ9_R0 = ");
    Serial.println(MQ9_R0);

    Serial.println(" ");

    DataString = '#';
    DataString += MQ9_RS_air;
    DataString += '#';
    DataString += MQ9_Clean_Ratio;
    DataString += '#';
    DataString += MQ9_R0;
    DataString += '#';
    
    DataString += ' ';
    DataString += '#';
        
    //********** MQ135-sensor *********//
    
    float MQ135_sensor_volt;
    float MQ135_RS_air;      
    float MQ135_R0;          
    float MQ135_sensorValue;
    float MQ135_CO2_ppm;
    float MQ135_Clean_Ratio;
    
    //--- Get a average data by testing 100 times ---//
    for(int x = 0 ; x < 100 ; x++)
    {
        MQ135_sensorValue = MQ135_sensorValue + analogRead(A3);
    }
    MQ135_sensorValue = MQ135_sensorValue/100.0;
    //-----------------------------------------------//

    MQ135_sensor_volt = MQ135_sensorValue/1024*5.0;
    MQ135_RS_air = (5.0-MQ135_sensor_volt)/MQ135_sensor_volt; // omit *RL
    
    MQ135_CO2_ppm = 402;  // Mean atmosfreic CO2 value (ppmv) as calibration reference 
    
    MQ135_Clean_Ratio = powf( MQ135_CO2_ppm / 109.1175 , 1 / -2.7927 );

    MQ135_R0 = MQ135_RS_air / MQ135_Clean_Ratio;

    Serial.print("MQ135_RS_air = ");
    Serial.print(MQ135_RS_air);
    Serial.println("kOhm");

    Serial.print("MQ135_Clean_Ratio = ");
    Serial.println(MQ135_Clean_Ratio);

    Serial.print("MQ135_R0 = ");
    Serial.println(MQ135_R0);

    Serial.println("#########################################");

    DataString += '#';
    DataString += MQ135_RS_air;
    DataString += '#';
    DataString += MQ135_Clean_Ratio;
    DataString += '#';
    DataString += MQ135_R0;
    DataString += '#';
        
    //********** END LOOP *********//

    //Serial.println(DataString);
    writeData();
    
    delay(1000);    
}

void writeData() {
  
    dataFile = SD.open(datasetNum, FILE_WRITE);
    dataFile.println(DataString);
    dataFile.close();
    datasetLength++;
   
  if (datasetLength >= 5000) {
    datasetLength = 0;
    datasetNumA++;
    datasetNum = String(datasetNumA) + ".txt";    
  }
  
}

/*
MQ-9:   calibrated R0 value = 0.06
MQ-135: calibrated R0 value = 7.9 
*/

