#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <SD.h>
#include <Arduino.h>
//#include "string.h"

// SD CARD Configuration
// Connected pin 5
int datasetLength = 0;
int datasetNumA = 0;
String datasetNum = "";                               
File dataFile;

// WIFI MODULE Configuration
SoftwareSerial WIFI_com(9, 10); 
String DataStringGPS;
String DataStringDUST;

// GPS Configuration
TinyGPSPlus GPS;
SoftwareSerial GPS_com(4, 3); //RXPin = 4, TXPin = 3;

String gpsStream = "";

// DUST SENSOR Configuration
#define LENG 31               //0x42 + 31 bytes equal to 32 bytes
unsigned char buf[LENG];
int PM01Value=0;              //define PM1.0 value of the air detector module
int PM2_5Value=0;             //define PM2.5 value of the air detector module
int PM10Value=0;              //define PM10 value of the air detector module

// Human interface system configuration
static const int buttonPin = 8;
static const int LEDPin = 6;
int buttonState = 0;

int STATE = 1;
// 0 = IDLE
// 1 = CHECK
// 2 = READ GPS DATA
// 3 = READ DUST SENSOR
// 4 = WRITE DATA ON SD CARD
// 5 = SEND DATA ON WIFI
// 6 = ERROR STATE

void setup() {

  pinMode(LEDPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  digitalWrite(LEDPin, HIGH);
  
  delay(5000);
  // SD CARD SETUP (on pin 5)

  datasetNumA = 0;
  datasetNum = String(datasetNumA) + ".txt";
  SD.begin(5);

  // Trova il file di log successivo
  int testSD = 1;
  while (testSD == 1) {
    datasetNum = String(datasetNumA) + ".txt";
    if (SD.exists(datasetNum)) {
      datasetNumA++;
    } else {
      testSD = 0;
    }
  }
  
  // DUST SENSOR SETUP

  Serial.begin(9600);
  Serial.setTimeout(1500);

  // GPS SETUP
  GPS_com.begin(9600);
    
  //delay(10000);
  digitalWrite(LEDPin, LOW);

  
  
  }

void loop() {
    switch (STATE) {
    case 0: // IDLE
      idle();
      break;
    case 1: // CHECK
      check();
      break;
    case 2: // READ GPS DATA
      readGPS();
      break;
    case 3: // READ DUST SENSOR
      readDust();
      break;
    case 4: // WRITE DATA ON SD CARD
      writeData();
      break;
    case 5: // SEND DATA OVER WIFI
      sendData();
      break;
    case 6: // ERROR STATUS
      errorStatus();
      break;
    }
  }


void idle() {
  STATE = 2;
}

void check() {
   for (int i=0; i < 5; ++i) {
      digitalWrite(LEDPin, HIGH);
      delay(1000);
      digitalWrite(LEDPin, LOW);
      delay(1000);
   }
   STATE = 0;
}

void readGPS() {
  while(GPS_com.available() > 0) {
    char t = GPS_com.read();
  }
  gpsStream ="";
  bool readData = true;
  char ch_data = "";
  int lineRead = 0;
  while (readData) {
    while (GPS_com.available()) {
      ch_data = GPS_com.read();
      gpsStream.concat(ch_data);
      if (ch_data == '\n') {
        lineRead++;
      }
      if (lineRead >= 8) {
        readData = false;
      }
    }
  }
  //Serial.println(gpsStream);
  
  for (int i = 0; i<gpsStream.length(); ++i) {
    char aa = gpsStream.charAt(i);
    GPS.encode(aa);
  }
  //Serial.print('\n');
      //GPS.encode(*gpsStream++);
      if (GPS.location.isValid()) {
        digitalWrite(LEDPin, HIGH);
      } else {
        digitalWrite(LEDPin, LOW);
      }
      DataStringGPS = GPS.location.isValid();
      DataStringGPS += '#';
      DataStringGPS += GPS.date.year();
      DataStringGPS += '#';
      DataStringGPS += GPS.date.month();
      DataStringGPS += '#';
      DataStringGPS += GPS.date.day();
      DataStringGPS += '#';
      DataStringGPS += GPS.time.hour();
      DataStringGPS += '#';
      DataStringGPS += GPS.time.minute();
      DataStringGPS += '#';
      DataStringGPS += GPS.time.second();
      DataStringGPS += '#';
      DataStringGPS += String(GPS.location.lat(), 8);
      DataStringGPS += '#';
      DataStringGPS += String(GPS.location.lng(), 8);
      DataStringGPS += '#';
   // }
  STATE = 3;
  
}


void readDust() {
  DataStringDUST = "";
  //while (DataStringDUST == "") {
    if(Serial.find(0x42)){                                  //start to read when detect 0x42
      Serial.readBytes(buf,LENG);

      if(buf[0] == 0x4d){
        if(checkValue(buf,LENG)){
          PM01Value=transmitPM01(buf); //count PM1.0 value of the air detector module
          PM2_5Value=transmitPM2_5(buf);//count PM2.5 value of the air detector module
          PM10Value=transmitPM10(buf); //count PM10 value of the air detector module
          DataStringDUST += PM01Value;
          DataStringDUST += "#";
          DataStringDUST += PM2_5Value;
          DataStringDUST += "#";
          DataStringDUST += PM10Value;
          DataStringDUST += "#";
        }           
      } 
    }
  //}
  STATE = 4;
}

void writeData() {
  
  //File dataFile = SD.open(datasetNum.c_str(), FILE_WRITE);
  dataFile = SD.open(datasetNum, FILE_WRITE);
    dataFile.print(DataStringGPS);
    dataFile.println(DataStringDUST);
    dataFile.close();
    STATE = 0;
    datasetLength++;
   if (!SD.exists(datasetNum)) {
    STATE = 6;
   }
  if (datasetLength >= 5000) {
    datasetLength = 0;
    datasetNumA++;
    datasetNum = String(datasetNumA) + ".txt";    
  }
}

void sendData() {
  Serial.print(DataStringGPS);
  Serial.println(DataStringDUST);
  STATE = 0;
}

void errorStatus() {
  for (int i=0; i< 20; ++i) {
  digitalWrite(LEDPin, HIGH);
  delay(100);
  digitalWrite(LEDPin, LOW);
  delay(100);
  }
  // Return to check mode
  STATE = 1;
  Serial.println("OK");
  
}


char checkValue(unsigned char *thebuf, char leng)
{  
  char receiveflag=0;
  int receiveSum=0;

  for(int i=0; i<(leng-2); i++){
  receiveSum=receiveSum+thebuf[i];
  }
  receiveSum=receiveSum + 0x42;
 
  if(receiveSum == ((thebuf[leng-2]<<8)+thebuf[leng-1]))  //check the serial data 
  {
    receiveSum = 0;
    receiveflag = 1;
  }
  return receiveflag;
}

int transmitPM01(unsigned char *thebuf)
{
  int PM01Val;
  PM01Val=((thebuf[3]<<8) + thebuf[4]); //count PM1.0 value of the air detector module
  return PM01Val;
}

//transmit PM Value to PC
int transmitPM2_5(unsigned char *thebuf)
{
  int PM2_5Val;
  PM2_5Val=((thebuf[5]<<8) + thebuf[6]);//count PM2.5 value of the air detector module
  return PM2_5Val;
  }

//transmit PM Value to PC
int transmitPM10(unsigned char *thebuf)
{
  int PM10Val;
  PM10Val=((thebuf[7]<<8) + thebuf[8]); //count PM10 value of the air detector module  
  return PM10Val;
}

void genDataName() {
  datasetNumA = datasetNumA + 1;
  datasetNum = String(datasetNumA) + ".txt";
}


